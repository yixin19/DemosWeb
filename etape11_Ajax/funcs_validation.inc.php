<?php

// fonction pour vérifier si la chaine ne contient que des caractères autorisés pour un nom, à savoir des caractères alphanumériques, espaces et des tirets et apostrophes
// renvoie FALSE si un caractère non autorisé est trouvé
function verifierCharInterdits($nomATester) {
    //Retourne FALSE s'il contient autre chose que des caractères autorisés, ou la chaine.
    return preg_match('/[^a-zA-Z\s\']+/', $nomATester) ? FALSE : $nomATester;
}

function validerNom($nom_a_tester){
    // on met en session pour réafficher le formulaire ou après validation, en protégeant contre les attaques XSS
    $_SESSION['nom']=htmlspecialchars($nom_a_tester);

    $nom_securise=filter_var($nom_a_tester, FILTER_CALLBACK, array('options' => 'verifierCharInterdits'));

    if ($nom_securise==FALSE){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['nom_erreur']="Votre nom ne doit contenir que des lettres, des chiffres, des tirets et des espaces";
        return FALSE;
    }
    return $nom_securise;
}


function validerTel($tel_dangereux){
    // si aucun numéro n'a été  fourni, on renvoie NULL pour insertion dans la BDD
    if(empty($tel_dangereux)){
        return NULL;
    }

    // sinon on met en session pour réafficher s'il est invalide, ou si un autre élément est invalide
    $_SESSION['tel']= htmlspecialchars($tel_dangereux);

    // le numéro doit contenir un entier
    $tel_securise=filter_var($tel_dangereux, FILTER_VALIDATE_INT);
    if(!$tel_securise){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['tel_erreur']="Le numéro de téléphone ne doit contenir que des chiffres";
        return FALSE;
    }

    // si le numéro ne contient que des chiffres, il doit en contenir obligatoirement 9
    if (strlen($tel_securise)!=9){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['tel_erreur']='Le numéro saisi doit contenir les 9 chiffres sans le 0';
        return FALSE;
    }

    // sinon tout va bien , on renvoie le numéro valide
    return $tel_securise;
}

function validerCourriel($courriel_a_tester){
    // si aucun numéro n'a été  fourni, on renvoie NULL pour insertion dans la BDD
    if(empty($courriel_a_tester)){
        return NULL;
    }

    // sinon on met en session pour réafficher s'il est invalide, ou si un autre élément est invalide
    $_SESSION['courriel']= htmlspecialchars($courriel_a_tester);

    // le courriel doit respecter les règles
    $courriel_securise=filter_var($courriel_a_tester, FILTER_VALIDATE_EMAIL);
    if(!$courriel_securise){
        // on indique qu'il y a au moins cette erreur pour que l'utilisateur corrige sa saisie
        $_SESSION['form_erreur']=TRUE;
        // on indique le problème pour permettre à l'utilisateur de faire sa correction
        $_SESSION['courriel_erreur']= "L'adresse doit respecter la forme xxx@yyy.zzz";
        return FALSE;
    }

    // sinon tout va bien , on renvoie le numéro valide
    return $courriel_securise;
}





?>
