    <footer id="basdepage" class="bg-secondary mt-3 pt-2">
        <div class="container">
            <div class="row">
                <div id="adresse" class="col-12 col-md-6">
                    <address>
                        <p>ESIGELEC : Technopôle du Madrillet<br>
                            Avenue Galilée - BP 10024<br>
                            76801 Saint-Etienne du Rouvray Cedex</p>
                    </address>
                </div>
                <div id="contact" class="col-12 col-md-6">
                    <p><i class="fa fa-phone fa-lg"></i>&nbsp;(+33) 02 32 91 58 58</p>
                    <p><i class="fa fa-envelope fa-lg"></i>&nbsp;tenier@esigelec.fr</p>
                </div>
            </div>
        </div>
    </footer>
    <!-- Ajout des scripts pour Bootstrap
jQuery, puis Popper.js, et enfin Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta/js/bootstrap.min.js" integrity="sha384-h0AbiXch4ZDo7tp9hKZ4TsHbi047NrKGLO3SEJAg45jXxnGIfYzk4Si90RDIqNm1" crossorigin="anonymous"></script>
